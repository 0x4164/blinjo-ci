<?php

class Bahan extends CI_Model{
    public function __construct(){
        //todo : r u admin?
    }

    public function all($where=null){
        $this->db->select('*,(select (b.price_ori+b.revenue)*((100-b.discount)/100)) as show_price');
        $this->db->from('bahan b');
        if($where!=null){
            $this->db->where($where);
        }
        $q=$this->db->get();
        return $q;
    }

    public function add($data=[]){
        $q=$this->db->insert('bahan',$data);
        $this->db->select('*,(select (b.price_ori+b.revenue)*(100-b.discount/100)) as show_price');
        $this->db->from('bahan b');
        $this->db->order_by('id','desc');
        $data=$this->db->get();
        return [$q,$data->row()];
    }
    
    public function update($data,$where){
        unset($data['id']);
        $this->db->update('bahan',$data,$where);

        // lq($this);
        // exit();
        return $this->db->affected_rows()>0;
    }
    
    public function delete($where){
        $where['id']="";
        $q=$this->db->delete('bahan',$where);
        return $this->db->affected_rows()>0;
    }
}