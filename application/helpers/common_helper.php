<?php

if(!function_exists('preout')){
    function preout($v=[]){
        echo "<pre>";
        var_dump($v);
        echo "</pre>";
    }
}

if(!function_exists('lq')){
    function lq($ins=null){
        echo $ins->db->last_query();
        exit();
    }
}

if(!function_exists('jsonize')){
    function jsonize($status,$msg,$var){
        $json=[
            "status"=>$status,
            "msg"=>$msg,
            "data"=>$var,
        ];
        return json_encode($json);
    }
}

if(!function_exists('span_status')){
    function span_status($v=1){
        $spans=[
            '0',
            '<span class="label label-warning">Pending</span>',
            '<span class="label label-success">Diterima</span>',
            '<span class="label label-danger">Ditolak</span>',
        ];
        return $spans[$v];
    }
}

if(!function_exists('tr')){
    function tr($v=1,$prop=""){
        $f="<tr %s>%s</tr>";
        $r=sprintf($f,$prop,$v);
        return $r;
    }
}

if(!function_exists('td')){
    function td($v=1,$prop=""){
        $f="<td %s>%s</td>";
        $r=sprintf($f,$prop,$v);
        return $r;
    }
}