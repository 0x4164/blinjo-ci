<?php
defined('JSON') OR define('JSON','Content-Type:text/json');

class Api extends CI_Controller{
    public function __construct(){
        parent::__construct();
        header(JSON);
        $this->load->model('bahan');
    }

    public function bahan_detail(){
        $where['id']=$this->input->get('id');
        $bahan=$this->bahan->all($where)->row();
        $data['bahan']=$bahan;
        echo jsonize(200,'success',$bahan);
    }

    public function bahan_insert(){
        $posts=$this->input->post();
        $data=$posts;
        $u=$this->bahan->add($data);

        echo jsonize(200,'success',$u);
    }

    public function bahan_update(){
        $posts=$this->input->post();
        $where['id']=$this->input->post('id');
        $data=$posts;
        $u=$this->bahan->update($data,$where);

        echo jsonize(200,'success',$u);
    }

    public function bahan_delete(){

    }
}