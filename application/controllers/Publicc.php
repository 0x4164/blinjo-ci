<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicc extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('bahan');
    }

	public function index(){
		echo "public dir";
    }
    
    public function harga(){
        $bahan=$this->bahan->all()->result();
        $data['bahan']=$bahan;
        $this->load->view('harga',$data);
    }
}
