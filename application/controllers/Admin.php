<?php
class Data{

}

class Admin extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $user=$this->session->userdata('user');
        // preout($user);
        if($user->loggedin!=true){
            redirect('login');
        }
        $this->load->model('bahan');
    }

    public function index(){
        $this->dashboard();
    }

    public function dashboard(){
        $bahan=$this->bahan->all()->result();
        $data['bahan']=$bahan;
        $this->load->view('dashboard',$data);
    }

    public function harga(){
        $bahan=$this->bahan->all()->result();
        $data['bahan']=$bahan;
        $this->load->view('harga',$data);
    }
}