-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 16, 2020 at 09:09 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blinjo`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'sayur',
  `price_ori` int(10) DEFAULT NULL,
  `qty` float DEFAULT 1 COMMENT 'kuantitas',
  `satuan` varchar(32) DEFAULT 'kg' COMMENT 'satuan metrik',
  `revenue` int(11) NOT NULL DEFAULT 0,
  `discount` float NOT NULL DEFAULT 0 COMMENT 'diskon',
  `add_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`id`, `name`, `type`, `price_ori`, `qty`, `satuan`, `revenue`, `discount`, `add_at`) VALUES
(1, '1', '1', 1, NULL, 'kg', 1, 0, '2020-01-16 10:21:50'),
(2, 'bahan 2', 'sayur', 20000, NULL, 'kg', 1000, 0, '2020-01-16 10:21:50'),
(3, '2', '2', 3, NULL, 'kg', 2, 0, '2020-01-16 13:56:48'),
(4, '3', '3', 3, NULL, 'kg', 3, 0, '2020-01-16 13:58:08'),
(5, '45', '4', 5, NULL, 'kg', 5, 0, '2020-01-16 13:59:48'),
(6, '2', '2', 3, NULL, 'kg', 3, 0, '2020-01-16 14:00:34'),
(7, '3', '3', 3, NULL, 'kg', 3, 0, '2020-01-16 14:02:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
