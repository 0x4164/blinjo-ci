<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Blinjo | Harga</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url()?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url()?>/assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url()?>/assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url()?>/assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url()?>/assets/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  .vanish{
      display:none;
  }
  </style>
</head>
<body class="hold-transition skin-green sidebar-collapse">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url()?>/assets/adminlte/index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Blinjo</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Blinjo</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
        
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <img src="<?= base_url()?>/assets/adminlte/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Mimin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?= base_url()?>/assets/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Mimin
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url()?>logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Daftar Harga | <a href="<?= base_url()?>harga">Lihat harga di Pelanggan</a>
      </h1>
      <button class="btn btn-success" data-toggle="modal" data-target="#modal-default" onclick="toAdd()">Tambah</button>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Daftar Harga</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8">
            <div class="box box-success">
                <div class="box-body">
                    <!-- to do : sort by -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="50">Id</th>
                                <th>name</th>
                                <th>type</th>
                                <th>price_ori</th>
                                <th>qty</th>
                                <th>satuan</th>
                                <th>revenue</th>
                                <th>discount</th>
                                <th>Harga akhir</th>
                                <th>add_at</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach($bahan as $b){ ?>
                              <tr id="r<?= $b->id?>">
                                  <td><?= $b->id?></td>
                                  <td><?= $b->name?></td>
                                  <td><?= $b->type?></td>
                                  <td><?= $b->price_ori?></td>
                                  <td><?= $b->qty?></td>
                                  <td><?= $b->satuan?></td>
                                  <td><?= $b->revenue?></td>
                                  <td><?= $b->discount?>%</td>
                                  <td><?= $b->show_price; ?></td>
                                  <td><?= $b->add_at?></td>
                                  <td>
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" onclick="detail(<?= $b->id ?>)">
                                      Edit
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="remove(<?= $b->id ?>)">
                                      Delete
                                    </button>
                                  </td>
                                  <!-- <td><?= "0x".dechex($b->price_ori) ?></td> -->
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
      <div class="modal fade" id="modal-default" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 id="mdltitle" class="modal-title">Bahan</h4>
            </div>
            <div class="modal-body">
              <p>-</p>
              <form id="form" class="form-horizontal" action="" type="update">
                <input type="hidden" name="id" value="">
                <div class="form-group">
                  
                    <label class="col-md-3 control-label" class="col-md-3 control-label" for="">Nama</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="name">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-md-3 control-label" class="col-md-3 control-label" for="">Tipe</label>
                <div class="col-md-9">
                  <input type="text" name="type" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Harga asli</label>
                  <div class="col-md-9">
                  <input type="text" name="price_ori" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Kuantitas per satuan</label>
                  <div class="col-md-9">
                  <input type="text" name="qty" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Satuan</label>
                  <div class="col-md-9">
                  <input type="text" name="satuan" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Untung</label>
                  <div class="col-md-9">
                  <input type="text" name="revenue" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Discount</label>
                  <div class="col-md-9">
                  <input type="text" name="discount" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="">Add_at</label>
                  <div class="col-md-9">
                    <input type="text" name="add_at" class="form-control" disabled>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Save changes">
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= base_url()?>/assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>/assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url()?>/assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url()?>/assets/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url()?>/assets/adminlte/dist/js/demo.js"></script>

<script>
  var baseurl="<?= base_url()?>";

  if (!String.prototype.format) {
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) { 
			return typeof args[number] != 'undefined'
				? args[number]
				: match
			;
			});
		};
	}

  var rowfmt="<td>{0}</td>\
                <td>{1}</td>\
                <td>{2}</td>\
                <td>{3}</td>\
                <td>{4}</td>\
                <td>{5}</td>\
                <td>{6}</td>\
                <td>{7}</td>\
                <td>{8}</td>\
                <td>{9}</td>\
                <td>\
                  <button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#modal-default\" onclick=\"detail({0})\">\
                    Edit\
                  </button>\
                  <button type=\"button\" class=\"btn btn-danger\" onclick=\"remove({0})\">\
                    Delete\
                  </button>\
                </td>";

  function toAdd(){
    $('#form').attr('type','add')
    reset()
  }

  function toUpdate(){
    $('#form').attr('type','update')
  }

  function reset(){
    $('input[name=id]').val('')
    $('input[name=name]').val('')
    $('input[name=type').val('')
    $('input[name=price_ori]').val('')
    $('input[name=qty]').val('')
    $('input[name=satuan]').val('')
    $('input[name=revenue]').val('')
    $('input[name=discount]').val('')
    $('input[name=add_at]').val('')
  }

  function detail(id){
    reset()
    toUpdate()
    $.ajax({
      url:baseurl+'api/bahan_detail',
      data:{id:id},
      success:function(data){
        console.log(data)
        $('input[name=id]').val(data.data.id)
        $('input[name=name]').val(data.data.name)
        $('input[name=type').val(data.data.type)
        $('input[name=price_ori]').val(data.data.price_ori)
        $('input[name=qty]').val(data.data.qty)
        $('input[name=satuan]').val(data.data.satuan)
        $('input[name=revenue]').val(data.data.revenue)
        $('input[name=discount]').val(data.data.discount)
        $('input[name=add_at]').val(data.data.add_at)
      }
    })
  }

  function remove(id){
    $.ajax({
      url:baseurl+'api/bahan_delete',
      data:{id:id},
      success:function(data){
        console.log(data)
        alert(data.msg)
      }
    })
  }

  var f1=null;
  $("#form").submit(function(e){
    e.preventDefault();
    var form = $(this);
    var type=$('#form').attr('type')
    var url='';

    if(type=="add"){
      url=baseurl+'api/bahan_insert'
    }else{
      url=baseurl+'api/bahan_update'
    }

    $.ajax({
      type: "POST",
      url:url,
      data:form.serialize(),
      success:function(data){
        console.log(data)
        console.log(form.serializeArray())
        f1=form.serializeArray();

        if(type=="add"){
          tab=$('tbody')
          rdata=data.data[1]
          tab.append("<tr>"+
            rowfmt.format(
              rdata.id,
              rdata.name,
              rdata.type,
              rdata.price_ori,
              rdata.qty,
              rdata.satuan,
              rdata.revenue,
              rdata.discount,
              parseInt(rdata.price_ori)+parseInt(rdata.revenue),
              rdata.add_at,
            )+"<tr>"
          )
        }else{
          id=f1[0].value
          $('#r'+id).html(rowfmt.format(
            f1[0].value,
            f1[1].value,
            f1[2].value,
            f1[3].value,
            f1[4].value,
            f1[5].value,
            f1[6].value,
            f1[7].value+"%",
            parseInt(f1[3].value)+parseInt(f1[4].value),
            new Date('Y-m-d H:i:s'),
          ))
        }
        alert(data.msg)
      }
    })
  })

  $("#forminsert").submit(function(e){
    e.preventDefault();
    var form = $(this);

    $.ajax({
      type: "POST",
      url:baseurl+'api/bahan_insert',
      data:form.serialize(),
      success:function(data){
        console.log(data)
        
        alert(data.msg)
      }
    })
  })

  /*
  [{"name":"id","value":"1"},{"name":"name","value":"bahan 1"},
  {"name":"type","value":"sayur"},{"name":"price_ori","value":"1111"},
  {"name":"revenue","value":"1000"}
   */

  //to do : simple cart system in text mode for whatsapp
</script>

</body>
</html>

